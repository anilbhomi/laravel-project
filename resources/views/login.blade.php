<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    
    <title>Document</title>
</head>
<body>
    <div class="container">
        @if (session('alert'))
            <div class="alert alert-success">
                {{ session('alert') }}
            </div>
        @endif
        <div class="form-section">
        <h1>Login</h1>
        <form action="/user/login" method="post"  class="form-horizontal">
        {{ csrf_field() }}
            <div class="form-group">
                <label class="control-label" for="email">Email Address</label>
                <input  class="form-control" type="email" name="login_email" id="email">
            </div>
            <div class="form-group">
                <label class="control-label" for="password">Password</label>
                <input  class="form-control" type="password" name="login_password" id="password">
            </div>
            <div class="form-group">
                <div class="col-lg-4 col-sm-offset-2">
                <input  class="form-control btn btn-success" type="submit" value="Login">
                </div>
            </div>
        </form>

       <center> <h4>OR</h4></center>

        <h1>Register</h1>
        <form action="/user/register" method="post"  class="form-horizontal">
        {{csrf_field()}}
        <div class="form-group">
                <label class="control-label" for="name">Full name</label>
                <input required class="form-control" type="text" name="name" id="name">
            </div>
            <div class="form-group">
                <label class="control-label" for="email">Email Address</label>
                <input required class="form-control" type="email" name="email" id="email">
            </div>
            <div class="form-group">
                <label class="control-label" for="password">Password</label>
                <input required  class="form-control" type="password" name="password" id="password">
            </div>
            <div class="form-group">
                <label class="control-label" for="cpassword">Confirm Password</label>
                <input required  class="form-control" type="password" name="cpassword" id="cpassword">
            </div>
            <div class="form-group">
                <div class="col-lg-4 col-sm-offset-2">
                <input  class="form-control  btn btn-primary" type="submit" value="Login">
                </div>
            </div>
        </form>
        </div>
    </div>
</body>
</html>