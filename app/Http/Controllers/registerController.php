<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\UserRegister;
class registerController extends Controller
{
    public function register(Request $req){
        $name =$req->name;
        $email=$req->email;
        $password=$req->password;
        $cpassword=$req->cpassword;
        if($password!=$cpassword){
            return redirect('/')->with('alert', 'Password Mismatch!');;
        }
        $data=array('name'=>$name,'email'=>$email,'password'=>bcrypt($password));
        
        UserRegister::create($data);
        return redirect('/')->with('alert', 'User Registered! Please Login');
    }

    public function login(Request $req){
       $email=$req->login_email;
       $password=$req->login_password;
       if (Auth::attempt(['email' => $email, 'password' => $password])) {
        return redirect()->route('home');  
        }
        return redirect()->back()->with('alert',UserRegister::where('email','=',$email)->exists());
       
    }
}
