<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    return view('login');
});
Route::get('home', ['as' => 'home_page', 'uses' => 'homeController@pageLoad']);
Route::post('/user/register','registerController@register')->name('user.register');
Route::post('/user/login','registerController@login')->name('user.login');
